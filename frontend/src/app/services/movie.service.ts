import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Movie} from "../interface/movie";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private baseUrl = environment.baseUrl;
  private relativePath: string = "/movies";

  constructor(private http: HttpClient) {
  }

  listMovie(): Observable<Array<Movie>> {
    return this.http.get<Array<Movie>>(`${this.baseUrl}${this.relativePath}`);
  }

  deleteMovie(id: Number | undefined): Observable<any> {
    return this.http.delete<Movie>(`${this.baseUrl}${this.relativePath}/${id}`);
  }

  saveOrUpdateMovie(movie: Movie) {
    if (movie.id) {
      console.log("update");
      return this.http.put<Movie>(`${this.baseUrl}${this.relativePath}`, movie);
    } else {
      console.log("save");
      return this.http.post<Movie>(`${this.baseUrl}${this.relativePath}`, movie);
    }
  }
}
