import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {MoviesComponent} from "./pages/movies/movies.component";
import {ReviewsComponent} from "./pages/reviews/reviews.component";

const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "movie", component: MoviesComponent},
  {path: "review", component: ReviewsComponent},
  {path: "**", redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
