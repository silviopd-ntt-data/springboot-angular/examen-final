export interface Review {
  id?: Number;
  comment: String;
  commentator: String;
  createAt: Date;
  movieId: Number;
}
