package com.example.api.Service;

import com.example.api.Dto.ReviewDto;
import com.example.api.Entity.Movie;
import com.example.api.Entity.Review;
import com.example.api.Repository.MovieRepository;
import com.example.api.Repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<Review> listar() {
        try {
            return reviewRepository.findAll();
        } catch (Exception ex) {
            throw new RuntimeException("Error al listar");
        }
    }

    @Override
    public Boolean registrar(ReviewDto review) {
        try {
            System.out.println(review);
            Optional<Movie> movieOptional = movieRepository.findById(review.getMovieId());
            System.out.println(movieOptional);
            if (!movieOptional.isPresent()) {
                return false;
            }

            Review reviewEntity = new Review();
            reviewEntity.setComment(review.getComment());
            reviewEntity.setCommentator(review.getCommentator());
            reviewEntity.setCreateAt(review.getCreateAt());
            reviewEntity.setMovie(movieOptional.get());

            reviewRepository.save(reviewEntity);
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("Error al registrar");
        }
    }

    @Override
    public Boolean modificar(ReviewDto review) {
        try {
            Optional<Review> reviewOptional = reviewRepository.findById(review.getId());
            Optional<Movie> movieOptional = movieRepository.findById(review.getMovieId());

            if (!reviewOptional.isPresent() || !movieOptional.isPresent()) {
                return false;
            }

            Review reviewEntity = new Review();
            reviewEntity.setId(review.getId());
            reviewEntity.setComment(review.getComment());
            reviewEntity.setCommentator(review.getCommentator());
            reviewEntity.setCreateAt(review.getCreateAt());
            reviewEntity.setMovie(movieOptional.get());

            reviewRepository.save(reviewEntity);
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("Error al modificar el registro");
        }
    }

    @Override
    public Boolean eliminar(Long id) {
        try {
            reviewRepository.deleteById(id);
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("Error al eliminar el registro");
        }
    }
}
