package com.example.api.Service;

import com.example.api.Dto.ReviewDto;
import com.example.api.Entity.Usuario;

public interface UsuarioService {

    public Boolean iniciarSesion(Usuario usuario);
}

