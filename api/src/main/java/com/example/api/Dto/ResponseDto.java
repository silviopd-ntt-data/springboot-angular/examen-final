package com.example.api.Dto;

import com.example.api.Entity.Movie;
import lombok.Data;
import org.springframework.http.ResponseEntity;

import java.util.List;

@Data
public class ResponseDto {

    private String message;
    private Integer status;
    private Object data;

    public static ResponseEntity<?> responseEntity(String message, Integer status, Object data) {
        ResponseDto responseDto = new ResponseDto();
        responseDto.setData(data);
        responseDto.setMessage(message);
        responseDto.setStatus(status);
        return ResponseEntity.status(responseDto.getStatus()).body(responseDto);
    }
}
